# CIRious Game- transition energetique


- Adrien Kubiak
- Valentin Lucas
- Alexandre Przyszczypkowski
- Aurélien Sas
- Théo Wattel

# Localisation du code
**Le code final se trouve sur la branch develop sous le commit "Suppression fichiers en trop" ou sur la branch Adrien sous le commit "Version_Release".**
Il se trouve aussi sur ce dossier Google Drive : [ici](https://drive.google.com/drive/folders/1U4kH6nUtqFKNxL2XY0GpQjSf0MCK7zOV).
Le jeu doit être executé avec Wamp en serveur local en lancant le fichier Energy_Guardian.html, nous contacter en cas de soucis.

# Idée générale

Thème : Transition énergétique
Message : L'exploitation d'énergies fossiles ne peut plus durer, il est temps de combattre ces dernières pour utiliser le plus possibles des sources d'énergie renouvelables.
Cible : Tous publics, jeu rapide à jouer.

# Catégorie du jeu
Shooter/FPS, associé à des questions décisives sur la transition énergétique qui ont une influence réelle sur le jeu.

# Scénario
Play and save the world...
Combattez les ennemis de la transition 
énergétique,les énergies fossiles doivent
être éliminées pour sauver le monde.
